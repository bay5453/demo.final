---
title: "demo.final"
author: "Vy"
date: "2022-12-18"
output: 
  html_document:
    keep_md: true
    toc: true
    toc_float: true
    number_sections: true
---
---


# Thema 1

Wir wollen etwas analysieren:

* x
* y
* z

Dabei nutzen wir die Methode **OLS**.

# Verwendeter Datensatz
Wir verwendet hier die Daten von Paket gfc.

```r
summary(gfc)
```

```
##    quarter               GDPE             GDPU      
##  Length:60          Min.   : 86.70   Min.   : 80.4  
##  Class :character   1st Qu.: 93.95   1st Qu.: 93.2  
##  Mode  :character   Median :102.95   Median :103.0  
##                     Mean   :101.86   Mean   :102.6  
##                     3rd Qu.:109.47   3rd Qu.:114.9  
##                     Max.   :115.70   Max.   :119.5
```
# Analyse
Zur Analyse definieren wir eine neue Variable:

```r
gfc$gdpegdpu <- gfc$GDPE/gfc$GDPU
```
Diese Variable ist interessant, weil:
1. Item 1
2. Item 2
3. Item 3
    + Item 3a
    + Item 3b
    
# Plot

```r
plot (gfc$gdpegdpu)
```

![](demo.final_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

